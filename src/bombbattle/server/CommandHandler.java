package bombbattle.server;

import bombbattle.server.entities.Player;

public class CommandHandler {
	public static boolean handleCommand(String cmd, String[] args){
		if(cmd.equalsIgnoreCase("exit") || cmd.equalsIgnoreCase("stop")){
			BombServer.running=false;
			return true;
		}
		if(cmd.equalsIgnoreCase("kick")){
			if(args != null){
				String name = args[0];
				Player ply = BombServer.getPlayer(name);
				if(ply != null){
					ply.kick("");
				}else{
					System.out.println("Player "+name+" not found.");
				}
			}else{
				System.out.println("Not enough arguments.");
			}
			return true;
		}
		
		if(cmd.equalsIgnoreCase("list")){
			System.out.println("Players:");
			System.out.println("---------------");
			for(Player ply: BombServer.getOnlinePlayers()){
				System.out.println(ply.getName());
			}
			return true;
		}
		
		return false;
	}
}

package bombbattle.server;

import bombbattle.server.entities.Entity;
import bombbattle.server.physics.BodyScheduler;
import bombbattle.server.physics.PhysicsObject;
import bombbattle.shared.packets.Packet_UpdateEnt;

public class UpdateServer extends Thread {
	
	public BombServer server;
	
	public static final double NANO_TO_BASE = 1.0e9;
	
	private long nextUpdate;
	
	public UpdateServer(BombServer server){
		this.server = server;
		this.nextUpdate = System.currentTimeMillis();
	}
	
	@Override
	public void run() {
		
		while(BombServer.running){
			if(this.nextUpdate > System.currentTimeMillis() ){
				try {
					Thread.sleep(this.nextUpdate-System.currentTimeMillis());
				} catch (InterruptedException e) {e.printStackTrace();}
			}
			
			
			this.nextUpdate = System.currentTimeMillis()+(1000L/60L);
			this.update();
		}
		BombServer.server.close();
		System.exit(0);
	}
	
	private long last;
	
	public void update(){
		
		if(!Game.running){
			if(BombServer.getOnlinePlayers().size() > 1){
				Game.start();
			}
		}else{
			if(BombServer.getOnlinePlayers().size() < 2){
				Game.end();
			}
			
			BodyScheduler.schedule();
			
			long time = System.nanoTime();
			long diff = time-this.last;
			this.last = time;
			
			double elapsed = (double)diff / NANO_TO_BASE;
			BombServer.world.update(elapsed);
			
			
			for(Entity ent:Entity.ents){
				ent.update();
			}
			
			for(Entity ent: Entity.ents){
				if(ent instanceof PhysicsObject){
					PhysicsObject po = (PhysicsObject)ent;
					if(po.getChanged()){
						Packet_UpdateEnt ue = new Packet_UpdateEnt();
						ue.x = po.getX();
						ue.y = po.getY();
						ue.id = po.getId().toString();
						BombServer.server.sendToAllUDP(ue);
						po.setChanged(false);
					}
				}
			}
			
		}
		
	}
	
}

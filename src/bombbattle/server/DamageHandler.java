package bombbattle.server;

import bombbattle.server.entities.Player;
import bombbattle.shared.packets.Packet_PlayerHealth;

public class DamageHandler {
	public static void handleDamage(Player ply, int damage){
		ply.setHealth(ply.getHealth()-damage);
		
		if(ply.getHealth() <= 0){
			ply.kill();
		}else{
			Packet_PlayerHealth ph = new Packet_PlayerHealth();
			ph.health = ply.getHealth();
			ph.id = ply.getId().toString();
			BombServer.server.sendToAllTCP(ph);
		}
		
		
	}
	
	public static void handleDamageFromExplosion(Player ply, float str){
		handleDamage(ply,(int)(str*25));
	}
	
	public static void handleDamageFromImpact(Player ply, int impact){
		
	}
}

package bombbattle.server;

import bombbattle.server.entities.Entity;
import bombbattle.server.entities.Player;
import bombbattle.shared.packets.Packet_AddEnt;
import bombbattle.shared.packets.Packet_Kick;
import bombbattle.shared.packets.Packet_Login;
import bombbattle.shared.packets.Packet_PlayerAction;
import bombbattle.shared.packets.Packet_Ready;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class ConnectionHandler extends Listener {
	
	@Override
	public void received(Connection connection, Object object) {
		CustomConnection cc = (CustomConnection)connection;
		
		if(object instanceof Packet_Login){
			Packet_Login pl = (Packet_Login)object;
			this.loginPlayer(cc,pl.name);
		}
		
		if(object instanceof Packet_PlayerAction){	
			Packet_PlayerAction pa = (Packet_PlayerAction)object;
			Player ply = (Player)cc.ent;
			ply.getInputHandler().handleInput(pa.action, ply);
		}
	}
	
	@Override
	public void disconnected(Connection connection) {
		CustomConnection cc = (CustomConnection)connection;
		if(cc.ent == null)return;
		cc.ent.remove(true);
	}

	private void loginPlayer(CustomConnection cc, String name){
		if(cc.ent != null)return;
		
		if(BombServer.getPlayer(name) != null){
			Packet_Kick pk = new Packet_Kick();
			pk.reason = "Duplicate Name.";
			BombServer.server.sendToTCP(cc.getID(), pk);
			return;
		}
		
		if(name.length() > 25){
			Packet_Kick pk = new Packet_Kick();
			pk.reason = "Name too long.";
			BombServer.server.sendToTCP(cc.getID(), pk);
			return;
		}
		
		for(Entity ent:Entity.ents){
			Packet_AddEnt ae = ent.getPacket();
			cc.sendTCP(ae);
		}
		cc.name = name;
		cc.ent = Player.spawnPlayer(cc,name);
		
		BombServer.server.sendToTCP(cc.getID(), new Packet_Ready());
	}
	
}

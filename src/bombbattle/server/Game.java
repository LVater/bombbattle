package bombbattle.server;

public class Game {
	public static boolean running;
	
	public static void start(){
		if(BombServer.server.getConnections().length > 1){
			Game.running = true;
		}
	}
	
	public static void end(){
		if(Game.running){
			BombServer.reset();
			Game.running = false;
			Game.start();
		}
	}
}

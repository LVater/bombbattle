package bombbattle.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import org.dyn4j.dynamics.World;
import bombbattle.server.entities.Entity;
import bombbattle.server.entities.Player;
import bombbattle.server.entities.level.Level;
import bombbattle.server.physics.BodyScheduler;
import bombbattle.server.physics.CollisionHandler;
import bombbattle.shared.Network;
import bombbattle.shared.packets.Packet_Reset;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

public class BombServer {
	
	public static boolean running=true;
	public static Server server;
	public static World world;
	
	public BombServer(){
		server = new Server(){
			@Override
			protected Connection newConnection() {
				return new CustomConnection();
			}
		};
		Network.register(server);
		server.addListener(new Listener.ThreadedListener(new ConnectionHandler()));
		try {
			server.bind(Network.port_tcp,Network.port_udp);
		} catch (IOException e) {e.printStackTrace();}
		server.start();
		BombServer.init();
		UpdateServer us = new UpdateServer(this);
		us.start();
		while(running){
			String line = null;
			if((line = BombServer.readLine()) != null){
				String[] splitted = line.trim().split(" ");
				String cmd = splitted[0];
				String[] args = null;
				if(splitted.length > 1){
					args = new String[splitted.length-1];
					for(int i=1;i<splitted.length;i++){
						args[i-1]=splitted[i];
					}
				}
				if(!CommandHandler.handleCommand(cmd, args)){
					System.out.println("Command not found.");
				}
			}
		}
	}
	
	public static void init(){
		BombServer.world = new World();
		world.setGravity(World.ZERO_GRAVITY);
		world.addListener(new CollisionHandler());
		Level.generateLevel(800/32, 600/32, 32);
	}
	
	public static String readLine(){
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = null;
		try {
			line = reader.readLine();
		} catch (IOException e) {
			//Ignore
		}
		return line;
	}
	
	public static Player getPlayer(String name){
		for(Connection con: BombServer.server.getConnections()){
			CustomConnection cc = (CustomConnection)con;
			if(cc.ent != null){
				if(cc.name.equalsIgnoreCase(name)){
					return (Player) cc.ent;
				}
			}
		}
		return null;
	}
	
	public static Set<Player> getOnlinePlayers(){
		Set<Player> players = new HashSet<Player>();
		for(Connection con: BombServer.server.getConnections()){
			CustomConnection cc = (CustomConnection)con;
			if(cc.ent != null){
				players.add((Player)cc.ent);
			}
		}
		return players;
	}
	
	public static void reset(){
		
		BodyScheduler.toAdd.clear();
		BodyScheduler.toRemove.clear();
		
		for(Entity ent: Entity.ents){
			ent.remove(false);
		}
		BombServer.world.removeAllBodies();
		BombServer.server.sendToAllTCP(new Packet_Reset());
		
		for(Connection connection: BombServer.server.getConnections()){
			CustomConnection cc = (CustomConnection)connection;
			cc.ent = Player.spawnPlayer(cc,cc.name);
		}
		
		BombServer.init();
	}
	
	public static void main(String[] args){
		new BombServer();
	}
}

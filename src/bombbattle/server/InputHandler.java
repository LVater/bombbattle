package bombbattle.server;

import bombbattle.server.entities.Bomb;
import bombbattle.server.entities.Player;

public class InputHandler {
	
	public boolean keyup;
	public boolean keydown;
	public boolean keyright;
	public boolean keyleft;
	public int lastX,lastY;
	
	public void handleInput(String action, Player ply){
		if(ply.getBody()==null)return;
		if(action.contains("keyup")){
			if(action.charAt(0) == '+'){
				//ply.vely=-ply.getSpeed();
				keyup = true;
				lastY = 0;
			}else{
				//if(ply.body.getVelocity().y < 0){
					//ply.vely=0;
				//}
				keyup = false;
			}
		}
		
		if(action.contains("keydown")){
			if(action.charAt(0) == '+'){
				//ply.vely=ply.getSpeed();
				keydown = true;
				lastY = 1;
			}else{
				//if(ply.body.getVelocity().y > 0){
					//ply.vely=0;
				//}
				keydown = false;
			}
		}
		
		if(action.contains("keyleft")){
			if(action.charAt(0) == '+'){
				//ply.velx=-ply.getSpeed();
				lastX = 0;
				keyleft = true;
			}else{
				//if(ply.body.getVelocity().x < 0){
					//ply.velx=0;
				//}
				keyleft = false;
			}
		}
		
		if(action.contains("keyright")){
			if(action.charAt(0) == '+'){
				//ply.velx=ply.getSpeed();
				keyright = true;
				lastX = 1;
			}else{
				//if(ply.body.getVelocity().x > 0){
				//	ply.velx=0;
				//}
				keyright = false;
			}
		}
		
		if(action.contains("keyaction")){
			new Bomb(ply.getX(),ply.getY(),15,15,3000);
		}
	}
}

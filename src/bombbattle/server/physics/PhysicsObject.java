package bombbattle.server.physics;

import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.geometry.Circle;
import org.dyn4j.geometry.Mass;
import org.dyn4j.geometry.Vector2;

import bombbattle.server.BombServer;
import bombbattle.server.entities.Entity;

public abstract class PhysicsObject extends Entity {
	
	private int tempx,tempy;
	protected boolean changed;
	protected Body body;
	
	public PhysicsObject(int x, int y, int width, int height) {
		super(x, y, width, height);
		BodyScheduler.add(this);
	}
	
	public PhysicsObject(int x, int y, int width, int height, boolean send, boolean createbody) {
		super(x, y, width, height, send);
		if(createbody){
			BodyScheduler.add(this);
		}
	}
	
	public PhysicsObject(int x, int y, int width, int height, boolean send) {
		super(x, y, width, height, send);
		BodyScheduler.add(this);
	}
	
	public void updateObject(){}
	
	@Override
	public void update(){
		this.tempx = x;
		this.tempy = y;
		
		if(this.body != null){
			if(body.getMass().isInfinite())return;
			
			Vector2 vel = body.getVelocity();
			Vector2 velinv = vel.getNegative();
			velinv = velinv.multiply(body.getFixture(0).getFriction()*80);
			body.applyForce(velinv);
			
			if(Math.abs(body.getVelocity().x+body.getVelocity().y) < 0.2d)body.setVelocity(new Vector2(0,0));
			
			
			this.x = (int) body.getWorldCenter().x;
			this.y = (int) body.getWorldCenter().y;
		}
		
		this.updateObject();
		
		int diffx = Math.abs(tempx-x);
		int diffy = Math.abs(tempy-y);
		if(diffx+diffy > 0){
			this.changed = true;
		}
	}
	
	public void createBody(){
		this.body = new Body();
		body.setUserData(this);
		body.translate(this.getX(), this.getY());
		BodyFixture f = new BodyFixture(new Circle(this.getWidth()/2));
		f.setDensity(1d);
		f.setFriction(0);
		f.setRestitution(0d);
		body.addFixture(f);
		body.setMass(Mass.Type.FIXED_ANGULAR_VELOCITY);
		body.setLinearDamping(0.1d);
		BombServer.world.addBody(body);
	}
	
	public void bodyCreated(){};
	
	public Body getBody(){
		return this.body;
	}
	
	public void setBody(Body body){
		this.body = body;
	}
	
	public boolean getChanged(){
		return this.changed;
	}
	
	public void setChanged(boolean changed){
		this.changed = changed;
	}
	
}

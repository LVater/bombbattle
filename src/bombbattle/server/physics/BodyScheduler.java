package bombbattle.server.physics;

import java.util.HashSet;
import java.util.Set;

import bombbattle.server.BombServer;

public class BodyScheduler {
	public static Set<PhysicsObject> toAdd = new HashSet<PhysicsObject>();
	public static Set<PhysicsObject> toRemove = new HashSet<PhysicsObject>();
	
	public static void schedule(){
		for(PhysicsObject po: toAdd){
			po.createBody();
			po.bodyCreated();
		}
		toAdd.clear();
		
		for(PhysicsObject po: toRemove){
			if(BombServer.world.containsBody(po.body)){
				BombServer.world.removeBody(po.body);
			}
		}
		toRemove.clear();
	}
	
	public static void add(PhysicsObject po){
		toAdd.add(po);
	}
	
	public static void remove(PhysicsObject po){
		toRemove.add(po);
	}
}

package bombbattle.server.physics;

import org.dyn4j.dynamics.contact.ContactListener;
import org.dyn4j.dynamics.contact.ContactPoint;
import org.dyn4j.dynamics.contact.PersistedContactPoint;
import org.dyn4j.dynamics.contact.SolvedContactPoint;

import bombbattle.server.entities.Player;
import bombbattle.server.entities.level.Tile;
import bombbattle.server.entities.level.TileType;

public class CollisionHandler implements ContactListener {

	@Override
	public boolean begin(ContactPoint point) {
		
		Player ply = null;
		Tile tile = null;
		
		if(point.getBody1().getUserData() instanceof Player && point.getBody2().getUserData() instanceof Tile){
			tile = (Tile) point.getBody2().getUserData();
			if(tile.getTileType() == TileType.MOVABLE){
				ply = (Player) point.getBody1().getUserData();
			}
		}
		
		if(point.getBody2().getUserData() instanceof Player && point.getBody1().getUserData() instanceof Tile){
			tile = (Tile) point.getBody1().getUserData();
			if(tile.getTileType() == TileType.MOVABLE){
				ply = (Player) point.getBody2().getUserData();
			}
		}
		
		if(ply != null && tile != null){
			double speed = Math.max(Math.abs(tile.getBody().getVelocity().x), Math.abs(tile.getBody().getVelocity().y));
			double pspeed = Math.max(Math.abs(ply.getBody().getVelocity().x), Math.abs(ply.getBody().getVelocity().y));
			
		}
		
		return true;
	}

	@Override
	public void end(ContactPoint point) {
	}

	@Override
	public boolean persist(PersistedContactPoint point) {
		return true;
	}

	@Override
	public void postSolve(SolvedContactPoint point) {
	}

	@Override
	public boolean preSolve(ContactPoint point) {
		return true;
	}

	@Override
	public void sensed(ContactPoint point) {
	}

}

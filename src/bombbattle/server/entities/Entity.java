package bombbattle.server.entities;

import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;

import bombbattle.server.BombServer;
import bombbattle.server.physics.BodyScheduler;
import bombbattle.server.physics.PhysicsObject;
import bombbattle.shared.packets.Packet_AddEnt;
import bombbattle.shared.packets.Packet_RemoveEnt;

public abstract class Entity {
	protected int x,y,width,height;
	protected UUID id;
	
	public static CopyOnWriteArraySet<Entity> ents = new CopyOnWriteArraySet<Entity>();
	
	public Entity(int x, int y, int width, int height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.id = UUID.randomUUID();
		ents.add(this);
		BombServer.server.sendToAllTCP(this.getPacket());
	}
	
	public Entity(int x, int y, int width, int height, boolean send){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.id = UUID.randomUUID();
		ents.add(this);
		if(send)BombServer.server.sendToAllTCP(this.getPacket());
	}
	
	public abstract void update();
	public abstract Packet_AddEnt getPacket();
	
	public void remove(boolean sendPacket){
		if(this instanceof PhysicsObject){
			PhysicsObject po = (PhysicsObject)this;
			if(po.getBody() != null)BodyScheduler.remove(po);
		}
		this.onRemove();
		if(Entity.ents.contains(this)){
			Entity.ents.remove(this);
		}
		
		if(sendPacket){
			Packet_RemoveEnt re = new Packet_RemoveEnt();
			re.id = this.getId().toString();
			BombServer.server.sendToAllTCP(re);
		}
	}
	
	public void onRemove(){}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
}

package bombbattle.server.entities.level;

import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.geometry.Mass;
import org.dyn4j.geometry.Rectangle;

import bombbattle.server.BombServer;
import bombbattle.server.physics.BodyScheduler;
import bombbattle.server.physics.PhysicsObject;
import bombbattle.shared.EntityType;
import bombbattle.shared.packets.Packet_AddEnt;

public class Tile extends PhysicsObject {
	
	private int tiletype,textureID;
	
	public Tile(int tiletype, int textureID, int x, int y, int width, int height) {
		super(x, y, width, height,false,false);
		this.tiletype = tiletype;
		this.textureID = textureID;
		if(tiletype > 0)BodyScheduler.add(this);
		BombServer.server.sendToAllTCP(this.getPacket());
	}

	@Override
	public Packet_AddEnt getPacket() {
		Packet_AddEnt ae = new Packet_AddEnt();
		ae.x = x;
		ae.y = y;
		ae.id = id.toString();
		ae.type = EntityType.TILE;
		ae.ex1 = this.textureID;
		return ae;
	}
	
	@Override
	public void createBody(){
		this.body = new Body();
		body.setUserData(this);
		body.translate(this.getX(), this.getY());
		BodyFixture f = new BodyFixture(new Rectangle(this.getWidth(), this.getHeight()));
		f.setDensity(50d);
		f.setFriction(500d);
		f.setRestitution(0d);
		
		body.addFixture(f);
		
		if(this.getTileType() == TileType.MOVABLE){
			body.setMass(Mass.Type.FIXED_ANGULAR_VELOCITY);
		}else{
			body.setMass(Mass.Type.INFINITE);
		}
		
		BombServer.world.addBody(body);
	}
	
	public int getTileType(){
		return this.tiletype;
	}
	
	public void setTileType(int tiletype){
		this.tiletype = tiletype;
	}
	
	public int getTextureID(){
		return this.textureID;
	}
	
	public void setTextureID(int textureID){
		this.textureID = textureID;
	}

}

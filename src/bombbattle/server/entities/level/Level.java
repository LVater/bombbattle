package bombbattle.server.entities.level;

import java.util.Random;


public class Level {
	public static int width,height,size;
	
	public static void generateLevel(int w, int h, int size){
		
		Random rand = new Random(System.currentTimeMillis());
		
		Level.width = w;
		Level.height = h;
		Level.size = size;
		
		for(int x = 0;x<w;x++){
			new Tile(TileType.SOLID,0,x*size+size/2,size/2,size,size);
			new Tile(TileType.SOLID,0,x*size+size/2,h*size+size/2,size,size);
		}
		for(int y = 0;y<h;y++){
			new Tile(TileType.SOLID,0,size/2,y*size+size/2,size,size);
			new Tile(TileType.SOLID,0,w*size-size+size/2,y*size+size/2,size,size);
		}
		
		for(int x=1;x<w-1;x++){
			for(int y=1;y<h-1;y++){
				int r = rand.nextInt(10);
				if(x != 1 || y != 1){
					if(r==0){
						new Tile(TileType.BREAKABLE,1,x*size+size/2,y*size+size/2,size,size);
					}else if(r==1){
						new Tile(TileType.MOVABLE,2,x*size+size/2,y*size+size/2,size,size);
					}
				}
			}
		}
	}
	
}

package bombbattle.server.entities.level;

public class TileType {
	public static final int DECORATION=0;
	public static final int SOLID=1;
	public static final int BREAKABLE=2;
	public static final int MOVABLE=3;
}

package bombbattle.server.entities;

import org.dyn4j.geometry.Vector2;

import com.esotericsoftware.kryonet.Connection;

import bombbattle.server.BombServer;
import bombbattle.server.CustomConnection;
import bombbattle.server.InputHandler;
import bombbattle.server.physics.PhysicsObject;
import bombbattle.shared.EntityType;
import bombbattle.shared.packets.Packet_AddEnt;
import bombbattle.shared.packets.Packet_Kick;

public class Player extends PhysicsObject {
	
	private String name;
	private int speed = 2;
	private int velx,vely;
	private int maxhealth, health;
	private InputHandler input;
	private Connection con;
	
	
	public Player(Connection con, String name, int x, int y, int width, int height,  int maxhealth) {
		super(x, y, width, height, false);
		this.name = name;
		this.setHealth(maxhealth);
		this.setMaxhealth(maxhealth);
		this.input = new InputHandler();
		BombServer.server.sendToAllTCP(this.getPacket());
		this.con = con;
	}
	
	public void updateObject(){
		if(body != null){
			
			if(input.keyleft && !input.keyright){
				this.velx = -speed;
			}
			
			if(input.keyright && !input.keyleft){
				this.velx = speed;
			}
			
			if(input.keyleft && input.keyright){
				if(input.lastX == 0){
					this.velx=-speed;
				}else{
					this.velx=speed;
				}
			}
			
			if(!input.keyleft && !input.keyright){
				velx=0;
			}
			
			if(input.keyup && !input.keydown){
				this.vely = -speed;
			}
			
			if(input.keydown && !input.keyup){
				this.vely = speed;
			}
			
			if(input.keyup && input.keydown){
				if(input.lastY == 0){
					this.vely=-speed;
				}else{
					this.vely=speed;
				}
			}
			
			if(!input.keyup && !input.keydown){
				vely=0;
			}
			
			this.body.setVelocity(new Vector2(velx*50,vely*50));
			
			
		}
	}
	
	@Override
	public void onRemove(){
		CustomConnection cc = (CustomConnection)this.con;
		cc.ent = null;
	}
	
	@Override
	public void bodyCreated(){
		body.setAutoSleepingEnabled(false);
		body.setAsleep(false);
		body.getFixture(0).setDensity(3d);
		body.getFixture(0).setFriction(0d);
	}

	public void kill(){
		this.remove(true);
	}
	
	public void kick(String reason){
		Packet_Kick pk = new Packet_Kick();
		pk.reason = reason;
		if(con != null)con.sendTCP(pk);
	}
	
	public static Player spawnPlayer(Connection con, String name){
		return new Player(con, name,44,44,20,20,100);
	}
	
	@Override
	public Packet_AddEnt getPacket() {
		Packet_AddEnt ae = new Packet_AddEnt();
		ae.x = x;
		ae.y = y;
		ae.id = id.toString();
		ae.type = EntityType.PLAYER;
		ae.ex1 = maxhealth;
		ae.name = name;
		return ae;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	public int getVelocityX(){
		return this.velx;
	}
	
	public void setVelocityX(int velx){
		this.velx = velx;
	}

	public int getVelocityY(){
		return this.vely;
	}
	
	public void setVelocityY(int vely){
		this.vely = vely;
	}

	public int getMaxhealth() {
		return maxhealth;
	}

	public void setMaxhealth(int maxhealth) {
		this.maxhealth = maxhealth;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public InputHandler getInputHandler() {
		return input;
	}

	public void setInputHandler(InputHandler input) {
		this.input = input;
	}
}

package bombbattle.server.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.RaycastResult;
import org.dyn4j.geometry.Vector2;

import bombbattle.server.BombServer;
import bombbattle.server.DamageHandler;
import bombbattle.server.entities.level.Tile;
import bombbattle.server.entities.level.TileType;
import bombbattle.server.physics.PhysicsObject;
import bombbattle.shared.EntityType;
import bombbattle.shared.packets.Packet_AddEnt;
import bombbattle.shared.packets.Packet_Explosion;

public class Bomb extends PhysicsObject {
	
	private long placeTime,fuse;
	
	public Bomb(int x, int y, int width, int height, long fuse) {
		super(x, y, width, height);
		this.placeTime = System.currentTimeMillis();
		this.fuse = fuse;
		
	}
	
	@Override
	public void updateObject(){
		
		if(body != null){
			if(!body.isActive()){
				boolean iscol = false;
				for(Entity ent: Entity.ents){
					if(ent instanceof Player){
						Player ply = (Player)ent;
						if(BombServer.world.getBroadphaseDetector().detect(ply.getBody(), this.getBody()))iscol=true;
					}
				}
				if(!iscol)this.body.setActive(true);
			}
		}
		
		if(placeTime+fuse < System.currentTimeMillis()){
			this.explode();
		}
	}
	
	public void explode(){
		Packet_Explosion pe = new Packet_Explosion();
		pe.x = this.x;
		pe.y = this.y;
		pe.size = 100;
		BombServer.server.sendToAllTCP(pe);
		
		this.createExplosionImpact(100);
		
		for(Entity ent: Entity.ents){
			if(ent instanceof Tile){
				Tile tile = (Tile)ent;
				if(tile.getTileType() == TileType.BREAKABLE){
					int distance = (int) Math.sqrt(Math.pow((tile.getX()-this.getX()),2)+Math.pow((tile.getY()-this.getY()), 2));
					if(distance <= 100){
						if(this.testRay(body, tile.getBody()))tile.remove(true);
					}
				}
			}
		}
		
		
		this.remove(true);
		
	}
	
	private void createExplosionImpact(float maxdistance){
		for(int i=0; i < BombServer.world.getBodyCount();i++){
			Body b = BombServer.world.getBody(i);
			if(b != null){
				if(b == this.body)continue;
				if(this.body == null)return;
				if(!this.testRay(this.body, b))continue;
				int maxforce = 20000;
				if(b.getUserData() instanceof Tile)maxforce*=5000;
				float distance = (float) b.getWorldCenter().distance(this.body.getWorldCenter());
				if(distance > maxdistance) {
					continue;
				}
				float str = (maxdistance-distance)/maxdistance;
				
				if(b.getUserData() instanceof Player){
					DamageHandler.handleDamageFromExplosion((Player)b.getUserData(), str);
				}
				
				float force = str*maxforce;
				float angle = (float) Math.atan2(b.getWorldCenter().y-this.getY(), b.getWorldCenter().x-this.getX());
				b.applyImpulse(new Vector2(Math.cos(angle)*force,Math.sin(angle)*force));
			}
		}
	}
	
	private boolean testRay(Body start, Body end){
		Vector2 s = start.getWorldCenter();
		Vector2 e = end.getWorldCenter();
		List<RaycastResult> results = new ArrayList<RaycastResult>();
		if(BombServer.world.raycast(s,e, true, true, results)){
			if(results.size() > 0){
				Collections.sort(results);
				for(RaycastResult rr: results){
					if(rr.getBody().getUserData() instanceof Tile){
						if(rr.getBody() != start && rr.getBody() != end)return false;
					}
					
				}
			}
		}
		return true;
	}
	
	@Override
	public void bodyCreated(){
		this.body.getFixture(0).setDensity(3f);
		body.getFixture(0).setFriction(3d);
		body.setActive(false);
		
		boolean iscol=false;
		for(Entity ent: Entity.ents){
				if(ent instanceof Bomb){
					Bomb bomb = (Bomb)ent;
					if(bomb == this)continue;
					if(BombServer.world.getBroadphaseDetector().detect(this.body, bomb.body))iscol=true;
				}
		}
		if(iscol)this.remove(true);
	}
	
	@Override
	public Packet_AddEnt getPacket() {
		Packet_AddEnt ae = new Packet_AddEnt();
		ae.x = x;
		ae.y = y;
		ae.ex1 = width;
		ae.id = id.toString();
		ae.type = EntityType.BOMB;
		return ae;
	}

}

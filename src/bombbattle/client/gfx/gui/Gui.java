package bombbattle.client.gfx.gui;

import java.util.HashSet;
import java.util.Set;

import org.newdawn.slick.Graphics;

public abstract class Gui {
	public Set<GuiElement> elements = new HashSet<GuiElement>();
	private GuiHandler handler;
	
	public Gui(GuiHandler handler){
		this.addElements();
		this.handler = handler;
	}
	
	public abstract void addElements();
	public abstract void render(Graphics g);
	public abstract void update();
	
	public void addElement(GuiElement element){
		elements.add(element);
	}
	
	public void renderElements(Graphics g){
		for(GuiElement element: elements){
			if(element.getVisible())element.render(g);
		}
	}
	
	public void updateElements(){
		for(GuiElement element: elements){
			if(element.getVisible())element.update();
		}
	}
	
	public GuiHandler getHandler(){
		return this.handler;
	}
	
	public void setHandler(GuiHandler handler){
		this.handler = handler;
	}
	
	public abstract int getID();
}

package bombbattle.client.gfx.gui;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;

import bombbattle.client.GameSettings;

public class GETextBox extends GuiElement {
	
	private String text="";
	private int max_length;
	private boolean active=false;
	
	public GETextBox(Gui gui, int id, int x, int y, int w, int h) {
		super(gui,id, x, y, w, h);
		this.max_length = w/9;
		
		GameSettings.gsm.getContainer().getInput().addKeyListener(new KeyListener() {
			
			@Override
			public void setInput(Input input) {
			}
			
			@Override
			public boolean isAcceptingInput() {
				return true;
			}
			
			@Override
			public void inputStarted() {
			}
			
			@Override
			public void inputEnded() {
			}
			
			@Override
			public void keyReleased(int key, char c) {
			}
			
			@Override
			public void keyPressed(int key, char c) {
				if(getActive()){
					if(key == Keyboard.KEY_LSHIFT || key == Keyboard.KEY_RSHIFT || key == Keyboard.KEY_LCONTROL)return;
					if(key == Keyboard.KEY_BACK){
						if(getText().length() > 0){
							setText(getText().substring(0, getText().length()-1));
						}
					}else{
						if(getText().length() < max_length){
							setText(getText()+c);
						}
					}
					getGui().getHandler().getListener().onCharacterTyped(GETextBox.this);
				}
				
			}
		});
	}
	
	@Override
	public void render(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(x, y, width, height);
		g.setColor(Color.black);
		g.drawString(this.getText(), x+2, y+height/2-8);
		if(active)g.fillRect(x+2+(text.length()*9), y+2, 1, height-3);
	}

	@Override
	public void update() {
		GameContainer container = GameSettings.gsm.getContainer();
		int mx = container.getInput().getAbsoluteMouseX();
		int my = container.getInput().getAbsoluteMouseY();
		if(mx > x && mx < x+width && my > y && my < y+height){
			if(container.getInput().isMouseButtonDown(0)){
					this.getGui().getHandler().getListener().onMouseClick(this);
				this.active = true;
			}
		}else{
			if(container.getInput().isMouseButtonDown(0))this.active=false;
		}
	}
	
	public String getText(){
		return this.text;
	}
	
	public void setText(String text){
		this.text = text;
	}
	
	public boolean getActive(){
		return this.active;
	}

}

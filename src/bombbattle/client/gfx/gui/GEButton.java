package bombbattle.client.gfx.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import bombbattle.client.GameSettings;

public class GEButton extends GuiElement {
	
	private boolean state;
	private String text;
	private Color color_normal,color_hover,color_text;
	
	public GEButton(Gui gui, int id, int x, int y, int w, int h, String text) {
		super(gui, id, x, y, w, h);
		this.text = text;
		this.color_normal = new Color(130,130,130);
		this.color_hover = new Color(190,190,190);
		this.color_text = new Color(255,255,255);
	}

	@Override
	public void render(Graphics g) {
		if(this.getState()){
			g.setColor(color_hover);
		}else{
			g.setColor(color_normal);
		}
		g.fillRect(x, y, width,height);
		g.setColor(color_text);
		g.drawString(this.text, this.x+this.width/2-(this.text.length()*5), this.y+this.height/2-8);
	}

	public boolean getState(){
		return this.state;
	}
	
	public void setState(boolean state){
		this.state = state;
	}

	@Override
	public void update() {
		GameContainer container = GameSettings.gsm.getContainer();
		int mx = container.getInput().getAbsoluteMouseX();
		int my = container.getInput().getAbsoluteMouseY();
		this.setState(false);
		if(mx > x && mx < x+width){
			if(my > y && my < y+height){
				this.setState(true);
				if(container.getInput().isMousePressed(0)){
					this.getGui().getHandler().getListener().onMouseClick(this);
				}
			}
		}
	}

}

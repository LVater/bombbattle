package bombbattle.client.gfx.gui;

public interface ActionListener {
	public void onMouseClick(GuiElement element);
	public void onCharacterTyped(GuiElement element);
}

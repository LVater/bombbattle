package bombbattle.client.gfx.gui;

import org.newdawn.slick.Graphics;

public abstract class GuiElement {
	
	protected int x,y,width,height,id;
	protected Gui gui;
	protected boolean visible=true;
	
	public GuiElement(Gui gui, int id, int x, int y, int w, int h){
		this.setX(x);
		this.setY(y);
		this.setWidth(w);
		this.setHeight(h);
		this.setID(id);
		this.gui = gui;
	}
	
	public abstract void render(Graphics g);
	public abstract void update();
	
	public int getX(){
		return this.x;
	}
	
	public void setX(int x){
		this.x = x;
	}
	
	public int getY(){
		return this.y;
	}
	
	public void setY(int y){
		this.y = y;
	}
	
	public int getWidth(){
		return this.width;
	}
	
	public void setWidth(int width){
		this.width = width;
	}
	
	public int getHeight(){
		return this.height;
	}
	
	public void setHeight(int height){
		this.height = height;
	}
	
	public int getID(){
		return this.id;
	}
	
	public void setID(int id){
		this.id = id;
	}
	
	public Gui getGui(){
		return this.gui;
	}
	
	public void setGui(Gui gui){
		this.gui = gui;
	}
	
	public boolean getVisible(){
		return this.visible;
	}
	
	public void setVisible(boolean visible){
		this.visible = visible;
	}
}

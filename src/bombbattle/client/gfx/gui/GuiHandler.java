package bombbattle.client.gfx.gui;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Graphics;

public class GuiHandler {
	public List<Gui> guis = new ArrayList<Gui>();
	public Gui currentGui;
	private ActionListener listener;
	
	public void addGui(Gui gui){
		if(guis.size() < 1)currentGui=gui;
		guis.add(gui);
	}
	
	public void switchGui(int id){
		for(Gui gui: guis){
			if(gui.getID() == id){
				this.currentGui = gui;
				return;
			}
		}
	}
	
	public void registerListener(ActionListener listener){
		this.listener = listener;
	}
	
	public ActionListener getListener(){
		return this.listener;
	}
	
	public void render(Graphics g){
		if(currentGui != null)currentGui.render(g);
	}
	
	public void update(){
		if(currentGui != null)currentGui.update();
	}
}

package bombbattle.client;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class ResourceManager {
	public static Image[] tiles = new Image[3];
	public static Image floor;
	
	public static void loadResources(){
		try {
			floor = new Image("resources/floor.png");
			tiles[0] = new Image("resources/solid.png");
			tiles[1] = new Image("resources/crate.png");
			tiles[2] = new Image("resources/box.png");
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}

package bombbattle.client;

import java.io.IOException;

import bombbattle.client.entities.Entity;
import bombbattle.client.entities.Player;
import bombbattle.client.states.gui.GuiMessage;
import bombbattle.shared.Network;
import bombbattle.shared.packets.Packet_Login;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Listener;

public class ServerConnection {
	public static Client client = new Client();
	public static boolean ready;
	public static Player player;
	
	public static void connect(){
		Network.register(client);
		client.addListener(new Listener.ThreadedListener(new NetworkHandler()));
		client.start();
		try {
			client.connect(5000, GameSettings.ip, Network.port_tcp,Network.port_udp);
		} catch (IOException e) {
			GuiMessage.showMessage("Connection failed");
		}
		
		Packet_Login pl = new Packet_Login();
		pl.name = GameSettings.name;
		client.sendTCP(pl);
	}
	
	public static void close(){
		if(client != null){
			client.stop();
			client.close();
			ready = false;
			player = null;
		}
		reset();
	}
	
	public static void reset(){
		Entity.ents.clear();
	}
	
	public static boolean isReady(){
		return ServerConnection.ready;
	}
	
	public static void setReady(boolean ready){
		ServerConnection.ready = ready;
	}
}

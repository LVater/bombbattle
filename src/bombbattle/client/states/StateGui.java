package bombbattle.client.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import bombbattle.client.ResourceManager;
import bombbattle.client.gfx.gui.GuiHandler;
import bombbattle.client.states.gui.GuiActionListener;
import bombbattle.client.states.gui.GuiMainMenu;
import bombbattle.client.states.gui.GuiMenu;
import bombbattle.client.states.gui.GuiMessage;

public class StateGui extends BBGameState {

	public static GuiHandler guihandler;
	
	@Override
	public void enter(GameContainer container, StateBasedGame game)
			throws SlickException {
	}

	@Override
	public void leave(GameContainer container, StateBasedGame game)
			throws SlickException {
	}
	
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		guihandler = new GuiHandler();
		GuiActionListener listener = new GuiActionListener();
		guihandler.registerListener(listener);
		guihandler.addGui(new GuiMainMenu(guihandler));
		guihandler.addGui(new GuiMessage(guihandler));
		guihandler.addGui(new GuiMenu(guihandler));
		ResourceManager.loadResources();
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		guihandler.render(g);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		guihandler.update();
	}

	@Override
	public int getID() {
		return 0;
	}

	

}

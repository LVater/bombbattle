package bombbattle.client.states.gui;

import bombbattle.client.GameSettings;
import bombbattle.client.ServerConnection;
import bombbattle.client.gfx.gui.ActionListener;
import bombbattle.client.gfx.gui.GEButton;
import bombbattle.client.gfx.gui.GETextBox;
import bombbattle.client.gfx.gui.GuiElement;
import bombbattle.client.states.StateGui;

public class GuiActionListener implements ActionListener {
	
	@Override
	public void onMouseClick(GuiElement element) {
		if(element instanceof GEButton){
			GEButton btn = (GEButton)element;
			if(btn.getID() == 0)GameSettings.gsm.enterState(1);
			if(btn.getID() == 1)System.exit(0);
			if(btn.getID() == 2)StateGui.guihandler.switchGui(0);
			if(btn.getID() == 3)GameSettings.gsm.enterState(1);
			if(btn.getID() == 4){
				GameSettings.gsm.enterState(0);
				StateGui.guihandler.switchGui(0);
				ServerConnection.close();
			}
		}
	}

	@Override
	public void onCharacterTyped(GuiElement element) {
		if(element instanceof GETextBox){
			GETextBox tb = (GETextBox)element;
			if(tb.getID() == 0)GameSettings.ip = tb.getText();
			if(tb.getID() == 1)GameSettings.name = tb.getText();
		}
	}
	
	

}

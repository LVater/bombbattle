package bombbattle.client.states.gui;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Graphics;

import bombbattle.client.GameSettings;
import bombbattle.client.gfx.gui.GEButton;
import bombbattle.client.gfx.gui.Gui;
import bombbattle.client.gfx.gui.GuiHandler;

public class GuiMenu extends Gui {

	public GuiMenu(GuiHandler handler) {
		super(handler);
	}

	@Override
	public void addElements() {
		this.addElement(new GEButton(this, 3, 400-100, 100, 200, 80,"Resume"));
		this.addElement(new GEButton(this, 4, 400-100, 200, 200, 80,"Disconnect"));
	}

	@Override
	public void render(Graphics g) {
		this.renderElements(g);
	}

	@Override
	public void update() {
		this.updateElements();
		if(GameSettings.gsm.getContainer().getInput().isKeyPressed(Keyboard.KEY_ESCAPE)){
			GameSettings.gsm.enterState(1);
		}
	}

	@Override
	public int getID() {
		return 2;
	}

}

package bombbattle.client.states.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import bombbattle.client.GameSettings;
import bombbattle.client.gfx.gui.GEButton;
import bombbattle.client.gfx.gui.Gui;
import bombbattle.client.gfx.gui.GuiHandler;
import bombbattle.client.states.StateGui;

public class GuiMessage extends Gui {
	
	public static String message;
	
	public GuiMessage(GuiHandler handler) {
		super(handler);
	}

	@Override
	public void addElements() {
		this.addElement(new GEButton(this,2, 400-100, 400, 200, 80, "Back to Menu"));
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.white);
		g.drawString(message, 400-(message.length()*5), 200);
		this.renderElements(g);
	}

	@Override
	public void update() {
		this.updateElements();
	}

	@Override
	public int getID() {
		return 1;
	}
	
	public static void showMessage(String message){
		if(GameSettings.gsm.getCurrentStateID() != 0){
			GameSettings.gsm.enterState(0);
		}
		GuiMessage.message = message;
		StateGui.guihandler.switchGui(1);
	}

}

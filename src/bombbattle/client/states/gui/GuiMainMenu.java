package bombbattle.client.states.gui;

import java.util.Random;

import org.newdawn.slick.Graphics;

import bombbattle.client.GameSettings;
import bombbattle.client.gfx.gui.GEButton;
import bombbattle.client.gfx.gui.GETextBox;
import bombbattle.client.gfx.gui.Gui;
import bombbattle.client.gfx.gui.GuiHandler;

public class GuiMainMenu extends Gui {

	public GuiMainMenu(GuiHandler handler) {
		super(handler);
	}

	@Override
	public void addElements() {
		this.addElement(new GEButton(this, 0, 400-100, 100, 200, 80,"Connect"));
		GETextBox tb = new GETextBox(this, 0, 400-100, 200, 200, 20);
		tb.setText("127.0.0.1");
		this.addElement(tb);
		
		tb = new GETextBox(this, 1, 400-100, 230, 200, 20);
		Random rand = new Random(System.currentTimeMillis());
		tb.setText("TestPlayer"+rand.nextInt(5000));
		GameSettings.name = tb.getText();
		this.addElement(tb);
		
		this.addElement(new GEButton(this, 1, 400-100, 300, 200, 80,"Exit"));
	}
	
	@Override
	public void render(Graphics g) {
		this.renderElements(g);
	}

	@Override
	public void update() {
		this.updateElements();
	}
	
	@Override
	public int getID() {
		return 0;
	}

}

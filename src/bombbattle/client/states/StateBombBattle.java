package bombbattle.client.states;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import bombbattle.client.GameSettings;
import bombbattle.client.InputManager;
import bombbattle.client.Renderer;
import bombbattle.client.ServerConnection;
import bombbattle.client.entities.Entity;
import bombbattle.client.states.gui.GuiMessage;

public class StateBombBattle extends BBGameState {
	
	private InputManager inp;
	
	@Override
	public void enter(GameContainer container, StateBasedGame game)
			throws SlickException {
		if(!ServerConnection.client.isConnected())ServerConnection.connect();
		
	}
	
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		inp = new InputManager(container.getInput());
	}
	
	@Override
	public void render(GameContainer container,StateBasedGame game, Graphics g)
			throws SlickException {
		
		if(ServerConnection.isReady()){
			if(GameSettings.gameRunning()){
				Renderer.render(g);
				if(!ServerConnection.player.getAlive()){
					g.setColor(Color.red);
					String text = "You are Dead";
					g.drawString(text, 400-(text.length()*10)/2, 300-10);
				}
			}else{
				g.setColor(Color.white);
				g.drawString("Waiting for more Players to join...", 250, 100);
				Renderer.renderPlayerList(g);
			}
			
		}else{
			g.setColor(Color.white);
			String text = "Connecting to "+GameSettings.ip+" as "+GameSettings.name+"...";
			g.drawString(text, 400-(text.length()*10)/2, 300);
		}
		
		
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		if(ServerConnection.isReady()){
			if(GameSettings.gameRunning()){
				if(ServerConnection.player.getAlive())inp.update();
				
				for(Entity ent: Entity.ents.values()){
					ent.update();
				}
			}
			
			if(container.getInput().isKeyPressed(Keyboard.KEY_ESCAPE)){
				StateGui.guihandler.switchGui(2);
				GameSettings.gsm.enterState(0);
			}
			
			if(container.getInput().isKeyDown(Keyboard.KEY_TAB)){
				Renderer.showPlayers = true;
			}else{
				Renderer.showPlayers = false;
			}
			
			if(!ServerConnection.client.isConnected()){
				GuiMessage.showMessage("Connection lost");
			}
		}
	}


	@Override
	public void leave(GameContainer container, StateBasedGame game) throws SlickException {
	}


	@Override
	public int getID() {
		return 1;
	}

}

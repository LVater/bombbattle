package bombbattle.client.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public abstract class BBGameState extends BasicGameState {

	@Override
	public abstract void init(GameContainer container, StateBasedGame game) throws SlickException;
	
	@Override
	public abstract void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException;

	@Override
	public abstract void update(GameContainer container, StateBasedGame game, int delta) throws SlickException;
	
	@Override
	public abstract void enter(GameContainer container, StateBasedGame game) throws SlickException;
	
	@Override
	public abstract void leave(GameContainer container, StateBasedGame game) throws SlickException;

	@Override
	public abstract int getID();
	
}

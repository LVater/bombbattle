package bombbattle.client;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import bombbattle.client.states.StateGui;
import bombbattle.client.states.StateBombBattle;

public class GameStateManager extends StateBasedGame {
	
	public GameStateManager() {
		super("BombBattle");
		GameSettings.gsm = this;
	}
	

	@Override
	public void initStatesList(GameContainer container) throws SlickException {
		this.addState(new StateGui());
		this.addState(new StateBombBattle());
	}
	
	public static void main(String[] args){
		try {
			GameStateManager gsm = new GameStateManager();
			AppGameContainer container = new AppGameContainer(gsm);
			container.setDisplayMode(800, 600, false);
			container.setTargetFrameRate(60);
			container.setAlwaysRender(true); //For Debugging
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}

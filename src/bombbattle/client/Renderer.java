package bombbattle.client;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import bombbattle.client.entities.Bomb;
import bombbattle.client.entities.Entity;
import bombbattle.client.entities.Explosion;
import bombbattle.client.entities.Player;
import bombbattle.client.entities.Tile;

public class Renderer {
	
	public static boolean showPlayers;
	
	public static void render(Graphics g){
		//g.fillRect(-GameSettings.cam.getX()+400, -GameSettings.cam.getY()+300, 800, 600, ResourceManager.floor, 0, 0);
		g.fillRect(-GameSettings.cam.getX() % 32, -GameSettings.cam.getY() % 32, 832, 632, ResourceManager.floor, 0, 0);
		Renderer.renderMap(g);
		Renderer.renderBombs(g);
		Renderer.renderExplosions(g);
		Renderer.renderPlayers(g);
		Renderer.renderPlayerList(g);
	}
	
	public static void renderPlayers(Graphics g){
		for(Entity ent: Entity.ents.values()){
			if(ent instanceof Player){
				ent.render(g);
			}
		}
	}
	
	public static void renderMap(Graphics g){
		for(Entity ent: Entity.ents.values()){
			if(ent instanceof Tile){
				ent.render(g);
			}
		}
	}
	
	public static void renderBombs(Graphics g){
		for(Entity ent: Entity.ents.values()){
			if(ent instanceof Bomb){
				ent.render(g);
			}
		}
	}
	
	public static void renderExplosions(Graphics g){
		for(Entity ent: Entity.ents.values()){
			if(ent instanceof Explosion){
				ent.render(g);
			}
		}
	}
	
	public static void renderPlayerList(Graphics g){
		if(!Renderer.showPlayers)return;
		int x = 400-100;
		int y = 200;
		for(Entity ent: Entity.ents.values()){
			if(ent instanceof Player){
				String name = ((Player)ent).getName();
				g.setColor(Color.gray);
				g.drawRect(x, y, 200, 20);
				g.setColor(Color.green);
				g.drawString(name, x+2, y+2);
				y+=20;
			}
		}
	}
	
	public static boolean isInView(int x, int y, int w, int h){
		if(x+w > 0 && x < 800){
			if(y+h > 0 && y < 600){
				return true;
			}
		}
		
		return false;
	}
	
	public static float curve(float newval, float oldval, float increments){
		int sign;
		float slip;
		sign=(int) Math.signum(oldval-newval);
		slip=(oldval-newval)/increments;
		oldval-=slip;
		if(sign!=Math.signum(oldval-newval))return newval;
		return oldval;
	}
}

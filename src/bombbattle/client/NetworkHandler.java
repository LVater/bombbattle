package bombbattle.client;

import java.util.UUID;

import bombbattle.client.entities.Bomb;
import bombbattle.client.entities.Entity;
import bombbattle.client.entities.Explosion;
import bombbattle.client.entities.Player;
import bombbattle.client.entities.Tile;
import bombbattle.client.states.gui.GuiMessage;
import bombbattle.shared.EntityType;
import bombbattle.shared.packets.Packet_AddEnt;
import bombbattle.shared.packets.Packet_Explosion;
import bombbattle.shared.packets.Packet_Kick;
import bombbattle.shared.packets.Packet_PlayerHealth;
import bombbattle.shared.packets.Packet_Ready;
import bombbattle.shared.packets.Packet_RemoveEnt;
import bombbattle.shared.packets.Packet_Reset;
import bombbattle.shared.packets.Packet_UpdateEnt;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class NetworkHandler extends Listener {
	
	@Override
	public void received(Connection connection, Object object) {
		if(object instanceof Packet_AddEnt){
			Packet_AddEnt ae = (Packet_AddEnt)object;
			this.addEntity(ae);
		}
		
		if(object instanceof Packet_UpdateEnt){
			Packet_UpdateEnt ue = (Packet_UpdateEnt)object;
			UUID uid = UUID.fromString(ue.id);
			if(!Entity.ents.containsKey(uid))return;
			Entity ent = Entity.ents.get(uid);
			ent.setX(ue.x);
			ent.setY(ue.y);
		}
		
		if(object instanceof Packet_RemoveEnt){
			Packet_RemoveEnt re = (Packet_RemoveEnt)object;
			UUID uid = UUID.fromString(re.id);
			if(Entity.ents.containsKey(uid)){
				
				Entity ent = Entity.ents.get(uid);
				if(ent instanceof Player){
					Player ply = (Player)ent;
					ply.setAlive(false);
				}
				
				Entity.ents.remove(uid);
			}
		}
		
		if(object instanceof Packet_Explosion){
			Packet_Explosion pe = (Packet_Explosion)object;
			UUID uid = UUID.randomUUID();
			new Explosion(uid,pe.x,pe.y,pe.size*2,pe.size*2);
		}
		
		if(object instanceof Packet_PlayerHealth){
			Packet_PlayerHealth ph = (Packet_PlayerHealth)object;
			UUID id = UUID.fromString(ph.id);
			if(Entity.ents.containsKey(id)){
				Entity ent = Entity.ents.get(id);
				if(!(ent instanceof Player))return;
				Player ply = (Player)ent;
				ply.setHealth(ph.health);
			}
		}
		
		if(object instanceof Packet_Kick){
			Packet_Kick pk = (Packet_Kick)object;
			GuiMessage.showMessage("You got kicked: "+pk.reason);
		}
		
		if(object instanceof Packet_Ready){
			ServerConnection.setReady(true);
		}
		
		if(object instanceof Packet_Reset){
			ServerConnection.reset();
		}
	}
	
	private void addEntity(Packet_AddEnt ae){
		if(ae.type==EntityType.PLAYER){
			Player player = (Player)Entity.getByID(UUID.fromString(ae.id));
			if(player != null){
				player.setAlive(true);
				return;
			}
			Player ply = new Player(UUID.fromString(ae.id),ae.x,ae.y,20,20,ae.ex1);
			ply.setName(ae.name);
			if(ae.name.equalsIgnoreCase(GameSettings.name)){
				ServerConnection.player=ply;
			}
		}
		if(ae.type==EntityType.BOMB)new Bomb(UUID.fromString(ae.id),ae.x,ae.y,ae.ex1,ae.ex1);
		if(ae.type==EntityType.TILE)new Tile(UUID.fromString(ae.id),ae.x,ae.y,32,32,ae.ex1);
	}

}

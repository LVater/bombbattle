package bombbattle.client;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Input;

import bombbattle.shared.packets.Packet_PlayerAction;

public class InputManager {
	private Input inp;
	
	private boolean key_up;
	private boolean key_down;
	private boolean key_left;
	private boolean key_right;
	private long key_action;
	
	public InputManager(Input inp){
		this.inp = inp;
	}
	
	public void update(){
		if(inp.isKeyDown(Keyboard.KEY_UP)){
			if(!key_up){
				this.sendAction("+keyup");
				key_up=true;
			}
		}else{
			if(key_up){
				this.sendAction("-keyup");
				key_up=false;
			}
		}
		
		if(inp.isKeyDown(Keyboard.KEY_DOWN)){
			if(!key_down){
				this.sendAction("+keydown");
				key_down=true;
			}
		}else{
			if(key_down){
				this.sendAction("-keydown");
				key_down=false;
			}
		}
		
		if(inp.isKeyDown(Keyboard.KEY_LEFT)){
			if(!key_left){
				this.sendAction("+keyleft");
				key_left=true;
			}
		}else{
			if(key_left){
				this.sendAction("-keyleft");
				key_left=false;
			}
		}
		
		if(inp.isKeyDown(Keyboard.KEY_RIGHT)){
			if(!key_right){
				this.sendAction("+keyright");
				key_right=true;
			}
		}else{
			if(key_right){
				this.sendAction("-keyright");
				key_right=false;
			}
		}
		
		if(inp.isKeyPressed(Keyboard.KEY_SPACE) && (key_action+300 < System.currentTimeMillis())){
				this.sendAction("+keyaction");
				this.key_action = System.currentTimeMillis();
		}
	}
	
	private void sendAction(String action){
		Packet_PlayerAction pa = new Packet_PlayerAction();
		pa.action = action;
		ServerConnection.client.sendTCP(pa);
	}
	
	
}

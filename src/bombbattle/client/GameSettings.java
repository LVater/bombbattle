package bombbattle.client;

import bombbattle.client.entities.Entity;
import bombbattle.client.entities.GameCam;
import bombbattle.client.entities.Player;


public class GameSettings {
	public static GameStateManager gsm;
	public static String name;
	public static String ip="127.0.0.1";
	public static GameCam cam = new GameCam(0,0);
	
	public static boolean gameRunning(){
		int count = 0;
		for(Entity ent: Entity.ents.values()){
			if(ent instanceof Player){
				if(((Player)ent).getAlive()){
					count++;
				}
			}
		}
		if(count > 1)return true;
		return false;
	}
}

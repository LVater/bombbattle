package bombbattle.client.entities;

public class GameCam {
	
	private int x,y;
	
	public GameCam(int x, int y){
		setX(x);
		setY(y);
	}
	
	public int getX(){
		return this.x;
	}
	
	public void setX(int x){
		this.x = x;
	}
	
	public int getY(){
		return this.y;
	}
	
	public void setY(int y){
		this.y = y;
	}
}

package bombbattle.client.entities;

import java.util.UUID;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import bombbattle.client.GameSettings;
import bombbattle.client.Renderer;

public class Player extends Entity {
	
	public static Player cl_player = null;
	
	private int health, maxhealth;
	private boolean alive=true;
	private String name;
	
	public Player(UUID id, int x, int y, int width, int height, int maxhealth) {
		super(id, x, y, width, height);
		this.setMaxhealth(maxhealth);
		this.setHealth(maxhealth);
	}

	@Override
	public void render(Graphics g) {
		if(name.equalsIgnoreCase(GameSettings.name)){
			g.setColor(Color.blue);
		}else{
			g.setColor(Color.red);
		}
		
		int dx = x-width/2-GameSettings.cam.getX()+400;
		int dy = y-height/2-GameSettings.cam.getY()+300;
		
		
		if(Renderer.isInView(dx-10, dy-10, width+20, height+20)){
			g.fillOval(dx, dy, width, height);
			
			g.setColor(Color.gray);
			g.drawRect(dx-((maxhealth/3)/4)-1, dy+height/2-20, maxhealth/3+1, 5);
			
			g.setColor(Color.red);
			g.fillRect(dx-((maxhealth/3)/4), dy+height/2-19, health/3, 4);
		}
		
		
	}

	@Override
	public void update() {
		if(name.equalsIgnoreCase(GameSettings.name)){
			int cx = (int) Renderer.curve(this.x,GameSettings.cam.getX(), 8);
			int cy = (int) Renderer.curve(this.y,GameSettings.cam.getY(), 8);
			GameSettings.cam.setX(cx);
			GameSettings.cam.setY(cy);
		}
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getMaxhealth() {
		return maxhealth;
	}

	public void setMaxhealth(int maxhealth) {
		this.maxhealth = maxhealth;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public boolean getAlive(){
		return alive;
	}
	
	public void setAlive(boolean alive){
		this.alive = alive;
	}

}

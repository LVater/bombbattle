package bombbattle.client.entities;

import java.util.UUID;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import bombbattle.client.GameSettings;
import bombbattle.client.Renderer;

public class Bomb extends Entity {

	public Bomb(UUID id, int x, int y, int width, int height) {
		super(id, x, y, width, height);
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.black);
		int dx = x-width/2-GameSettings.cam.getX()+400;
		int dy = y-height/2-GameSettings.cam.getY()+300;
		if(Renderer.isInView(dx, dy, width, height))g.fillOval(dx, dy, width, height);
	}

	@Override
	public void update() {
	}

}

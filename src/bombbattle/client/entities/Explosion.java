package bombbattle.client.entities;

import java.util.UUID;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import bombbattle.client.GameSettings;

public class Explosion extends Entity {

	private long startTime;
	
	public Explosion(UUID id, int x, int y, int width, int height) {
		super(id, x, y, width, height);
		this.startTime = System.currentTimeMillis();
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.red);
		int dx = x-width/2-GameSettings.cam.getX()+400;
		int dy = y-height/2-GameSettings.cam.getY()+300;
		g.drawOval(dx, dy, width, width);
	}



	@Override
	public void update() {
		if(startTime+500 < System.currentTimeMillis()){
			if(Entity.ents.containsKey(this.getId())){
				Entity.ents.remove(this.getId());
			}
		}
	}

}

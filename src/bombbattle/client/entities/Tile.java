package bombbattle.client.entities;

import java.util.UUID;

import org.newdawn.slick.Graphics;

import bombbattle.client.GameSettings;
import bombbattle.client.Renderer;
import bombbattle.client.ResourceManager;

public class Tile extends Entity {
	
	private int textureID;
	
	public Tile(UUID id, int x, int y, int width, int height, int textureID) {
		super(id, x, y, width, height);
		this.textureID = textureID;
		if(textureID > ResourceManager.tiles.length-1)textureID=0;
	}

	@Override
	public void render(Graphics g) {
		int dx = x-width/2-GameSettings.cam.getX()+400;
		int dy = y-height/2-GameSettings.cam.getY()+300;
		if(Renderer.isInView(dx, dy, width, height))g.drawImage(ResourceManager.tiles[textureID], dx, dy);
		
	}

	@Override
	public void update() {
	}

}

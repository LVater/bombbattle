package bombbattle.client.entities;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.newdawn.slick.Graphics;

public abstract class Entity {
	protected int x,y,width,height;
	protected UUID id;
	
	public static ConcurrentHashMap<UUID,Entity> ents = new ConcurrentHashMap<UUID,Entity>();
	
	public Entity(UUID id, int x, int y, int width, int height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.id = id;
		ents.put(id, this);
	}
	
	public abstract void render(Graphics g);
	public abstract void update();
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
	public static Entity getByID(UUID id){
		if(Entity.ents.containsKey(id)){
			return Entity.ents.get(id);
		}
		return null;
	}
}

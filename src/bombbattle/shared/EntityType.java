package bombbattle.shared;

import bombbattle.server.entities.Bomb;
import bombbattle.server.entities.Entity;
import bombbattle.server.entities.Player;
import bombbattle.server.entities.level.Tile;

public class EntityType {
	public static final int TILE=0;
	public static final int PLAYER=1;
	public static final int BOMB=2;
	
	public static int fromServerEnt(Entity ent){
		if(ent instanceof Player){
			return EntityType.PLAYER;
		}else if(ent instanceof Bomb){
			return EntityType.BOMB;
		}else if(ent instanceof Tile){
			return EntityType.TILE;
		}
		return 0;
	}
}

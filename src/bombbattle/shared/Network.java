package bombbattle.shared;

import bombbattle.shared.packets.Packet_AddEnt;
import bombbattle.shared.packets.Packet_Explosion;
import bombbattle.shared.packets.Packet_Kick;
import bombbattle.shared.packets.Packet_Login;
import bombbattle.shared.packets.Packet_PlayerAction;
import bombbattle.shared.packets.Packet_PlayerHealth;
import bombbattle.shared.packets.Packet_Ready;
import bombbattle.shared.packets.Packet_RemoveEnt;
import bombbattle.shared.packets.Packet_Reset;
import bombbattle.shared.packets.Packet_UpdateEnt;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;

public class Network {
	
	public final static int port_tcp=5000;
	public final static int port_udp=5001;
	
	public static void register(EndPoint ep){
		Kryo kryo = ep.getKryo();
		kryo.register(Packet_Login.class);
		kryo.register(Packet_Ready.class);
		kryo.register(Packet_PlayerAction.class);
		kryo.register(Packet_AddEnt.class);
		kryo.register(Packet_RemoveEnt.class);
		kryo.register(Packet_UpdateEnt.class);
		kryo.register(Packet_Explosion.class);
		kryo.register(Packet_PlayerHealth.class);
		kryo.register(Packet_Kick.class);
		kryo.register(Packet_Reset.class);
	}
}
